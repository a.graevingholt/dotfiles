# dotfiles
Just my dotfiles for zsh with a little install script to install os specific powerlevel9k stuff

## Requirements
- `powerline-status` for vim and tmux powerline -> `pip3 install powerline-status --user`
- `coderay` for the `ccat` alias
- `fzf` for fuzzy search and as search menu in i3wm

## Information
Automatically installs:
- [antigen for zsh](https://github.com/zsh-users/antigen)
- `sp` if using Linux for adding Spotify to the [custom_now_playing](https://github.com/bhilburn/powerlevel9k/wiki/User-Segments#current-itunesosx--spotifyubuntu-track) segment
- `nodejs`, `npm`, `nvm` and the packages `commitizen`,`cz-conventional-changelog` and `ncu`
.zshrc also installs parts from [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) and the [powerlevel9k theme](https://github.com/bhilburn/powerlevel9k)
(Disclaimer, as of 2/21/18, powerlevel9k is installed from my personal fork [here on github](https://github.com/sambadevi/powerlevel9k))

## Git Setup
You can call `configure-git.sh` with your username and email to set the global git configuration:
```
./configure-git.sh "myUsername" "myEmail"
```
If you have a gpg key (or multiple) set, the script will automatically assign the first key to sign your commits

Afterwards check your git config with `git config -l`

## Recommendations
- dark theme preset from [materialshell](https://github.com/carloscuesta/materialshell) for iTerm2 on OSX
- Patched RobotoMono for nerd-fonts
