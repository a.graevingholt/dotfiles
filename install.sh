#!/usr/bin/env zsh
files=( ".tmux.conf" ".zsh_addons" ".zshrc" ".czrc" ".powerlevelrc" ".Xresources" ".xprofile" ".xinitrc")
bins=( "nowplaying" "github_issues" "itunes-track" )
folders=(".vim")
configs=("i3" "polybar" "gtk-3.0" "rofi")
completions=( "_venv" "_pwl")
scripts=( "fzf_cmd.sh" "fzf_window.sh")


mkdir -p $HOME/.dotfiles/{bin,backup,.comp,scripts,.configs,.vim}

local timestamp="$(date +%Y%m%d-%H:%M:%S)"

echo "Creating backup in ~/.dotfiles/backup/$timestamp"
rsync -a ~/.dotfiles ~/.dotfiles/backup/$timestamp --exclude backup

echo "Copying files..."
for file in ${files[@]}; do
    cp -f $file $HOME/.dotfiles/$file
    ln -fs $HOME/.dotfiles/$file $HOME/$file
done
cp ./.configs/cmus/wal.theme ~/.config/cmus/wal.theme

echo "Linking folders..."
for folder in ${folders[@]}; do
  cp -rf $folder/ ~/.dotfiles/
  ln -nsf ~/.dotfiles/$folder ~/
done
for config in ${configs[@]}; do
  cp -rf .configs/$config/ ~/.dotfiles/.configs
  ln -nsf ~/.dotfiles/.configs/$config ~/.config/
done
echo "Copying binaries..."
for bin in ${bins[@]}; do
    cp -f bin/$bin $HOME/.dotfiles/bin/$bin
    chmod +x $HOME/.dotfiles/bin/$bin
done

echo "Copying completions..."
for completion in ${completions[@]}; do
    cp -f .comp/$completion $HOME/.dotfiles/.comp/$completion
    chmod +x $HOME/.dotfiles/.comp/$completion
done

echo "Copying scripts..."
for script in ${scripts[@]}; do
    cp -f scripts/$script $HOME/.dotfiles/scripts/$script
    chmod +x $HOME/.dotfiles/scripts/$script
done
