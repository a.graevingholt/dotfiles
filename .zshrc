fpath=(~/.dotfiles/.comp $fpath)

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source ~/.powerlevelrc

source ~/.dotfiles/bin/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle docker
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting

# Use bhilburn's repo @ master
antigen theme bhilburn/powerlevel9k powerlevel9k
antigen apply

source ~/.zsh_addons
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# Load local modifications (if available)
test -f ~/.zshrc.local && source ~/.zshrc.local
test -f ~/.zsh_addons.local && source ~/.zsh_addons.local
test -f ~/.powerlevelrc.local && source ~/.powerlevelrc.local
